import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestLambdaDemo {

    public static final Human human1 = new Human("Москвичёв", "Максим", "Алексеевич",  20, "Male");
    public static final Human human2 = new Human("Москвичёв", "Пётр", "Петрович",  25, "Male");
    public static final Human human3 = new Human("Устьянцев", "Андрей", "Андреевич",18, "Male");
    public static final Human human4 = new Human("Королёва", "Евгения", "Андреевна",60, "Female");
    public static final Student student1 = new Student("Морозова", "Екатерина", "Игоревна", 18, "Female", "ОмГУ", "ИМИТ", "Прикладная математика и информатика");
    public static final Student student2 = new Student("Тимофеева", "Полина", "Вячеславовна", 20, "Female", "МГУ", "Мех. мат", "Математика");




    @Test
    void testStringLength() {
        assertNull(LambdaDemo.strLen.apply(null));
        assertEquals(0, LambdaDemo.strLen.apply(""));
        assertEquals(6, LambdaDemo.strLen.apply("строка"));
        assertEquals(21, LambdaDemo.strLen.apply("строка строка строка."));
    }


    @Test
    void testFirstChar() {
        assertNull(LambdaDemo.firstChar.apply(null));
        assertNull(LambdaDemo.firstChar.apply(""));
        assertEquals('7', LambdaDemo.firstChar.apply("7"));
        assertEquals('С', LambdaDemo.firstChar.apply("Слова какие-то"));
    }


    @Test
    void testNoSpaces() {
        assertTrue(LambdaDemo.strWithoutSpaces.apply("фылдова287кш4рк98н8"));
        assertFalse(LambdaDemo.strWithoutSpaces.apply("Ух пробел"));
    }


    @Test
    void testCountCommaSeparatedWords() {
        assertNull(LambdaDemo.firstChar.apply(null));
        assertEquals(null, LambdaDemo.countOfWords.apply(""));
        assertEquals(1, LambdaDemo.countOfWords.apply("1"));
        assertEquals(3, LambdaDemo.countOfWords.apply("Слово, запятая,сильно. очень"));
        assertEquals(2, LambdaDemo.countOfWords.apply("1,,2"));
    }


    @Test
    void testAge() {
        assertNull(LambdaDemo.ageOfHuman.apply(null));
        assertEquals(20, LambdaDemo.ageOfHuman.apply(human1));
        assertEquals(60, LambdaDemo.ageOfHuman.apply(human4));
        assertEquals(20, LambdaDemo.ageOfHuman.apply(student2));
    }


    @Test
    void testSameSecondName() {
        assertFalse(LambdaDemo.haveSameSurname.apply(null, null));
        assertFalse(LambdaDemo.haveSameSurname.apply(human1, null));
        assertFalse(LambdaDemo.haveSameSurname.apply(null, human2));
        assertTrue(LambdaDemo.haveSameSurname.apply(human1, human2));
        assertFalse(LambdaDemo.haveSameSurname.apply(human1, student1));
    }


    @Test
    void testJoinNames() {
        assertEquals("Москвичёв Максим Алексеевич", LambdaDemo.joinNames.apply(human1));
        assertEquals("Устьянцев Андрей Андреевич", LambdaDemo.joinNames.apply(human3));
        assertEquals("Морозова Екатерина Игоревна", LambdaDemo.joinNames.apply(student1));
    }


    @Test
    void testMakeOlder() {
        Human olderHuman1 = new Human("Москвичёв", "Максим", "Алексеевич",  21, "Male");
        Human olderHuman4 = new Human("Королёва", "Евгения", "Андреевна", 61, "Female");

        assertNull(LambdaDemo.makeOlder.apply(null));
        assertEquals(21, LambdaDemo.makeOlder.apply(human1).getAge());
        assertEquals(61, LambdaDemo.makeOlder.apply(human4).getAge());
        assertEquals(20, human1.getAge());
        assertEquals(60, human4.getAge());
    }


    @Test
    void testCheckYoungerThan() {
                 assertTrue(LambdaDemo.checkYoungerThan.check(150,
                        human1, human2, human4));
                 assertTrue(LambdaDemo.checkYoungerThan.check(28,
                        human1, human2, human3));
                 assertTrue(LambdaDemo.checkYoungerThan.check(23,
                        human1, student1, student2));
                assertFalse(LambdaDemo.checkYoungerThan.check(22,
                        human1, human2, human3));
    }
}
