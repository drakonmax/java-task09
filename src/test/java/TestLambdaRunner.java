import org.junit.jupiter.api.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;


public class TestLambdaRunner {
    @Test
    void testStringLambdasRun() {
        Function<String, Integer> lambda = LambdaDemo.strLen;
        String s1 = "Строчка";

        assertEquals(7, LambdaRunner.stringLambdasRun(lambda, s1));

    }

    @Test
    void testHumanLambdasRun() {
        Function<Human, Integer> lambda = LambdaDemo.ageOfHuman;
        Human human = new Human("Чичиков", "Акакий", "Акакиевич", 25, "Male");

        assertEquals(25, LambdaRunner.humanLambdasRun(lambda, human));
    }

    @Test
    void testHumanBiFuncRun() {
        BiFunction<Human, Human, Boolean> lambda = LambdaDemo.haveSameSurname;
        Human human = new Human("Москвичёв", "Евгений", "Иванович", 53, "Male");
        Human human1 = new Human("Москвичёв", "Аркадий", "Иванович", 21, "Male");

        assertTrue(LambdaRunner.humanBiFuncRun(lambda, human, human1));
    }

    @Test
    void testPersonPlusPlusRun() {
        Function<Human, Human> lambda = LambdaDemo.makeOlder;
        Human human = new Human("Чичиков", "Акакий", "Акакиевич", 25, "Male");

        assertEquals(26, LambdaRunner.personPlusPlusRun(lambda, human).getAge());
    }

    @Test
    void testCheckAgeRun() {
        ILambdaDemo lambda = LambdaDemo.checkYoungerThan;
        Human h1 = new Human("Косолапова", "Наталья", "Викторовна", 42, "Female");
        Human h2 = new Human("Манина", "Марина", "Евгеньевна", 17, "Female");
        Human h3 = new Human("Кашина", "Людмила", "Олеговна", 30, "Female");

        assertTrue(LambdaRunner.checkAgeRun(lambda,45, h1, h2, h3));
    }
}
