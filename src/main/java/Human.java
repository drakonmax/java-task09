public class Human {
    private String surname;
    private String name;
    private String patronymicName;
    private int age;
    private String sex;

    public Human(String surname, String name, String patronymicName, int age, String sex) {
        this.surname = surname;
        this.name = name;
        this.patronymicName = patronymicName;
        this.age = age;
        this.sex = sex;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}