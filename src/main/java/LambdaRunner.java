import org.jetbrains.annotations.NotNull;

import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaRunner {

    /**
     *Для 3.1-3.4
     */

    public static Object stringLambdasRun(@NotNull Function<String, ?> lambda, String str){
        return lambda.apply(str);
    }

    /**
     *Для 3.5, 3.7
     */
    public static <T extends Human> Object humanLambdasRun(@NotNull Function<T,?> lambda, T human){
        return lambda.apply(human);
    }

    /**
     *Для 3.6
     */
    public static <T extends Human> Boolean humanBiFuncRun(@NotNull BiFunction<T, T, Boolean> lambda,
                                                           T human, T human1){
        return lambda.apply(human, human1);
    }

    /**
     *Для 3.8
     */
    public static Human personPlusPlusRun(@NotNull Function<Human, Human> lambda, Human human){
        return lambda.apply(human);
    }

    /**
     *Для 3.9
     */
    public static boolean checkAgeRun(@NotNull ILambdaDemo lambda,int maxAge, Human human, Human human1, Human human2){
        return lambda.check(maxAge, human, human1, human2);
    }
}
