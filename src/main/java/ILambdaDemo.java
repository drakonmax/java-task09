@FunctionalInterface
public interface ILambdaDemo {
    boolean check(int age, Human human1, Human human2, Human human3);
}