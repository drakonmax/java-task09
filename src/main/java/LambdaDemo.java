import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaDemo{

    public static final  Function<String, Integer> strLen = str -> str == null ? null : str.length();

    public static final Function<String, Character> firstChar = str -> (str == null || str.isEmpty()) ? null : str.charAt(0);

    public static final Function<String, Boolean> strWithoutSpaces = str -> str == null || str.isEmpty() ? null : !str.contains(" ");

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Пустая строка - это не слово.
    */
    //Исправлено.
    public static final Function<String, Integer> countOfWords = str -> {
        if (str == null || str.isEmpty()) {return null;}
        String[] arr = str.split(",");
        int count = 0;
        for (String temp: arr){
            if (temp.equals("")){
                count++;
            }
        }
        return str.split(",").length-count;
    };


    public static final Function<Human, Integer> ageOfHuman = human -> human == null ? null : human.getAge();

    public static final BiFunction<Human, Human, Boolean> haveSameSurname = (human1, human2) -> human1 != null && human2 != null &&
            human1.getSurname().equals(human2.getSurname());

    public static final Function<Human, String> joinNames =
            x -> x.getSurname() + " " + x.getName() + " " + x.getPatronymicName();

    public static final Function<Human, Human> makeOlder = human -> human == null? null :
            new Human(human.getSurname(), human.getName(), human.getPatronymicName(), human.getAge()+1, human.getSex());

    public static final ILambdaDemo checkYoungerThan = (maxAge, human1, human2, human3) ->
            human1.getAge() < maxAge && human2.getAge() < maxAge && human3.getAge() < maxAge;



}
